    managers.job:activate_job ( "jewelry_store" )
    Global.game_settings.level_id = "jewelry_store"
    Global.game_settings.mission = managers.job:current_mission()
    Global.game_settings.difficulty = "normal"
    local level_id = tweak_data.levels:get_index_from_level_id( Global.game_settings.level_id )
    local job_id = tweak_data.narrative:get_index_from_job_id( managers.job:current_job_id() )
    managers.network.matchmake:create_lobby( { numbers = { level_id, "normal", "friends", nil, nil, 1, 1, 1 } } )
