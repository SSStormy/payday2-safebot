#!/bin/bash
CMD='xdotool windowfocus --sync 0 key --window 0 --clearmodifiers --delay 100 '

echo $CMD
echo "Starting loop"
sleep 1


findpid()
{
    PID=`xdotool search --name "PAYDAY 2" | tail -n1`
    echo "Found pid $PID"
    CMD='xdotool windowfocus --sync '
    CMD+="$PID"
    CMD+=' key --window '
    CMD+="$PID"
    CMD+=' --clearmodifiers --delay 100 '
}

findpid

lreset=false

while true; do
    echo 'Checking for crashes.'
    crash=$(($CMD o) 2>&1)
    if grep -q "X Error of failed" <<<$crash; then
        echo "PAYDAY 2 crashed"
        # launch payday
        ~/.steam/steam/steamapps/common/PAYDAY\ 2/payday2_release
        sleep 5
        findpid
        $CMD Return
        sleep 4
        $CMD Escape
        lreset=true
        continue
    fi
    
    sleep 2

    echo "Starting."

    if $lreset ; then
        $CMD Down
        $CMD Down
        $CMD Return
        $CMD Right
        $CMD Right
        $CMD Down
        $CMD Down
        $CMD Return
        $CMD Escape
        $CMD Up
        $CMD Up
        $CMD Up
        lreset=false
    else
        $CMD Up
    fi


    $CMD Return

    echo "Waiting for loading screen"
    sleep 8

    echo "Preplanning"
    $CMD Return

    echo "Waiting for bain to stfu"
    sleep 11

    echo "Forcewin"
    $CMD i

    echo "waiting for money & exp"
    sleep 14
    $CMD Return

    echo "Card screen"
    sleep 1

    echo "Cards flipped."
    $CMD Return

    echo "Waiting for cards"
    sleep 11
    $CMD Return

    echo "Waiting for loading screen"
    sleep 5
    echo done
done
