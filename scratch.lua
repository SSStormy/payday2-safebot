local safe = "buck_01"
local safe_tweak = tweak_data.economy.safes[safe]
local content_tweak = safe_tweak and tweak_data.economy.contents[safe_tweak.content]
local safe_instance_id = managers.blackmarket:tradable_instance_id("safes", safe)
     managers.blackmarket:tradable_receive_item_by_instance_id(safe_instance_id)
     Steam:inventory_reward_open(safe_instance_id, content_tweak.def_id, reward_unlock_callback)
