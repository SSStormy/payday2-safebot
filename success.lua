
function secure(name)
    managers.loot:server_secure_loot(name, managers.money:get_bag_value(name), false )
end

function getallsecureables( check_active_bag, check_active_small_loot ) 
        local unit_list = World:find_units_quick( "all" ) 
        for _,unit in ipairs( unit_list ) do 
                --[[Check for securable items.]] 
                if ( unit:carry_data() ) then --[[Check if it's a carryable item.]] 
                        --io.write("Found Carry " .. unit:carry_data():carry_id() .. " Active: " .. tostring(unit:interaction():active()) .. "\n") --[[Debug]] 
                        if ( ((unit:interaction():active() == true) and (check_active_bag == true)) or (check_active_bag == false) ) then 
                                secure(unit:carry_data():carry_id()) 
                        end 
                end 
                if ( tostring(unit:name()) == "Idstring(@IDd40d72ec32a655be@)" --[[Weapon Case]] ) then 
                        if ( ((unit:interaction():active() == true) and (check_active_bag == true)) or (check_active_bag == false) ) then 
                                --io.write("Found weapon\n") --[[Debug]] 
                                secure("weapon") --[[I have to nest the secure("weapon") call in here, because it's just the weapon case.]] 
                        end 
                end 
                --[[Check for grabbable items]] 
                if ( unit:base() ) then --[[Some units don't have a base.]] 
                        if ( unit:base().small_loot ) then --[[Check if it's a grabbable.]] 
                                --if ( ((unit:interaction():active() == true) and (check_active_small_loot == true)) or (check_active_small_loot == false) ) then --[[TODO: Come up with a reason for this check.]] 
                                        unit:interaction():interact(managers.player:player_unit()) 
                                        --io.write("Found Small Loot " .. unit:base().small_loot .. " Active: " .. tostring(unit:interaction():active()) .. "\n") --[[Debug]] 
                                --end 
                        end 
                end 
                if ( tostring(unit:name()) == "Idstring(@IDe8088e3bdae0ab9e@)" --[[Yellow Package]] or
					 tostring(unit:name()) == "Idstring(@ID05956ff396f3c58e@)" --[[Blue Package]] or
					 tostring(unit:name()) == "Idstring(@IDc90378ad89058c7d@)" --[[Purple Package]] or
					 tostring(unit:name()) == "Idstring(@ID96504ebd40f8cf98@)" --[[Red Package]] or
					 tostring(unit:name()) == "Idstring(@IDb3cc2abe1734636c@)" --[[Green Package]] or
					 tostring(unit:name()) == "Idstring(@ID22c41a3f6d26a887@)" --[[Airplane Keys]] or
                     tostring(unit:name()) == "Idstring(@ID5422d8b99c7c1b57@)" --[[Card]] or 
                     tostring(unit:name()) == "Idstring(@ID39a568911e4f1318@)" --[[Generic Intel]] or 
                     tostring(unit:name()) == "Idstring(@IDdf59d98261985354@)" --[[Blueprints]] ) then 
                    --[[TODO: Look in to finding a more generic way of discriminating against mission objectives.]] 
                    if ( ((unit:interaction():active() == true) and (check_active_small_loot == true)) or (check_active_small_loot == false) ) then 
						--io.write("Found Intel " .. unit:name() .. "\n") --[[Debug]] 
						unit:interaction():interact(managers.player:player_unit())
                    end 
                end 
        end 
end 


if (Network:is_server() == true) then 
        if managers.platform:presence() == "Playing" then
            local num_winners = managers.network:session():amount_of_alive_players()
            managers.network:session():send_to_peers("imission_ended", true, num_winners)
             game_state_machine:change_state_by_name("victoryscreen", { 
                 num_winners = num_winners,
                 personal_win = alive(managers.player:player_unit())
             })
		end 
    end  
